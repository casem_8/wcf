﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MiPrimerServicioWCF
{
    public class CalculadoraService : ICalculadora
    {
        public int Sumar(int numero1, int numero2)
        {
            return numero1 + numero2;
        }
    }
}
